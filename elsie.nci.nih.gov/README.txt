To update the Olson database, unpack tzcode20???.tar.gz and tzdata20???.tar.gz
into the src directory and update the OLSON_VERSION variable in
../src/pytz/__init__.py and the EXPECTED_VERSION variable in
../src/pytz/tests/test_tzinfo.py.
